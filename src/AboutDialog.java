/** About.java */

import javax.swing.JOptionPane;

/** 
 * For the Chief Musician. A Psalm of David. How long, O Jehovah? wilt thou forget me for ever? 
 * How long wilt thou hide thy face from me? 2 How long shall I take counsel in my soul, 
 * Having sorrow in my heart all the day? How long shall mine enemy be exalted over me? 
 * Consider and answer me, O Jehovah my God: Lighten mine eyes, lest I sleep the sleep of death; 
 * Lest mine enemy say, I have prevailed against him; Lest mine adversaries rejoice when I am moved. 
 * But I have trusted in thy lovingkindness; My heart shall rejoice in thy salvation. 
 * I will sing unto Jehovah, Because he hath dealt bountifully with me.
 *
 * About dialog window
 * 
 * @author dum8d0g 
 * @version 1.0rc1
 * @created April 2007
 */
public class AboutDialog{
    
    public AboutDialog() {
        JOptionPane.showMessageDialog(null, "Czvubber Instant Messenger version" +
                "\nDeveloped by pools and dum8d0g as school project \nat CVUT FEL 2007");
    }

}
