/**
 * Quit.java
 */



/**
 * Quit is our class for ending the program. First it tests if the connection
 * is established an then it ends the program.
 *
 * @author pavel
 * @version 1.0rc1
 * @created April 2007
 */
public class Quit {
    
    /** Method Quit for quitting ;-) */
    public Quit() {
        if(new QuitDialog().getResult()){
            System.out.println("Czvubber is shuting down..");
            // We get the connection object
            Connection connection = Connection.get();
            // Then we chceck if it's connectet or disconnected
            if( !connection.isNull() && connection.isConnected() ){
                // If we are connected we simply disconnet and shut down
                connection.close();
                System.out.println("Connection was closed.");
                System.out.println("Bye... And don't kill litle Kitty's");
                System.exit(0);
            } else{
                // Elsewhere we shloud shut down easily
                System.out.println("Bye... And don't kill litle Kitty's");
                System.exit(0);
            }
        }
    }
}