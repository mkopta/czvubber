/*
 * ConnectionErrorDialog.java
 */


import javax.swing.JOptionPane;

/**
 * For the Chief Musician; set to the Sheminith. A Psalm of David. Help, Jehovah; 
 * For the godly man ceaseth; For the faithful fail from among the children of men. 
 * They speak falsehood every one with his neighbor: With flattering lip, 
 * and with a double heart, do they speak. Jehovah will cut off all flattering lips, 
 * The tongue that speaketh great things; Who have said, With our tongue will we prevail; 
 * Our lips are our own: Who is lord over us? Because of the oppression of the poor, 
 * because of the sighing of the needy, Now will I arise, saith Jehovah; 
 * I will set him in the safety he panteth for. The words of Jehovah are pure words; 
 * As silver tried in a furnace on the earth, Purified seven times. Thou wilt keep them, 
 * O Jehovah, Thou wilt preserve them from this generation for ever. 
 * The wicked walk on every side, When vileness is exalted among the sons of men.
 *
 * Connection Error dialog window
 * 
 * @author pavel
 * @version 1.0rc1
 * @created April 2007
 */
public class ConnectionErrorDialog {
    
    /** Creates a new instance of ConnectionErrorDialog */
    public ConnectionErrorDialog() {
     JOptionPane.showMessageDialog(null, "Connection to the server failed. Try to check you name and password.");
    }
    
}
