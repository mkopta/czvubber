/** ConfigWindow.java */

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.EventListener;
import javax.swing.*;

/**
 * Config window is gui for configuring config.xml
 * - it can change user, password, server and source
 *
 * @author pavel
 * @version 1.0rc1
 * @created April 2007
 */
public class ConfigWindow extends JFrame {
    
    // declaration of JLabel components
    private JLabel userLabel, passLabel, serverLabel, sourceLabel;
    // declaration of JButton components
    private JButton cancelButton, saveButton;
    // declaration of JTextField components
    private JTextField userField, serverField, sourceField;
    // declaration of JPasswordField componnent
    private JPasswordField passField;
    // declaration of Container for our components
    private Container pane;
    // declaration of JFrame for our Container
    private JFrame mainFrame;
    
    /**
     *  Constructor of ConfigWindow
     */
    public ConfigWindow() {
        components();
        fill();
    }
    
    /**
     *  components() puts together all of our components and puts them into the JFrame.
     *  - JLayout is GridLayout
     */
    private void components() {
        
        // JFrame initialization and setup
        mainFrame = new JFrame("Configuration");
        mainFrame.setDefaultLookAndFeelDecorated(true);
        mainFrame.addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent windowEvent) {}
            public void windowClosed(WindowEvent windowEvent) {}
            public void windowClosing(WindowEvent windowEvent) {
                Register.get().switchStatusOfConfigWindow();
            }
            public void windowDeactivated(WindowEvent windowEvent) {}
            public void windowDeiconified(WindowEvent windowEvent) {}
            public void windowIconified(WindowEvent windowEvent) {}
            public void windowOpened(WindowEvent windowEvent) {}
        });
        mainFrame.setSize(300, 140);
        
        // inicialization and setup of our components
        cancelButton = new JButton("Cancel");
        saveButton = new JButton("Save and close");
        userLabel = new JLabel("User:");
        userField = new JTextField();
        passLabel = new JLabel("Password:");
        serverLabel = new JLabel("Server:");
        serverField = new JTextField();
        passField = new JPasswordField();
        sourceLabel = new JLabel("Source:");
        sourceField = new JTextField();
        
        // sets up action listener on cancel button
        cancelButton.setToolTipText("Discard all changes");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        
        // sets up action listener on save button
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        
        // Container inicialization and layout setup
        pane = new Container();
        pane.setLayout(new GridLayout(0,2));
        
        // adding components to container
        pane.add(userLabel);
        pane.add(userField);
        pane.add(passLabel);
        pane.add(passField);
        pane.add(serverLabel);
        pane.add(serverField);
        pane.add(sourceLabel);
        pane.add(sourceField);
        pane.add(saveButton);
        pane.add(cancelButton);
        
        // adding container to frame
        mainFrame.add(pane);
        
        // making the frame visible ;)
        mainFrame.setVisible(true);
        Register.get().switchStatusOfConfigWindow();
    }
    
    /** 
     * Method for filling up the fields.
     */
    public void fill(){
        Config config = Config.get();
        userField.setText(config.getUser());
        passField.setText(config.getPass());
        serverField.setText(config.getServer());
        sourceField.setText(config.getSource());
    }
    
    /**
     * Saving method, its simple ActionEvent.
     */
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Config config = Config.get();
        config.setUser(userField.getText());
        config.setPass(passField.getPassword());
        config.setServer(serverField.getText());
        config.setSource(sourceField.getText());
        config.save();
        mainFrame.dispose();
        Register.get().switchStatusOfConfigWindow();
    }
    
    /** 
     * Exiting method, its simple Action event.
     */
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        mainFrame.dispose();
        Register.get().switchStatusOfConfigWindow();
    }
}
