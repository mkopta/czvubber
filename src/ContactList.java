/** ContactList.java */

import java.util.Iterator;
import javax.swing.AbstractListModel;
import org.jivesoftware.smack.*;

/** 
 * Contact list is the model for our contact list. We are using this model
 * in our MainWindow in View package for our JList contactList.
 *
 * @author dum8d0g
 * @version 1.0rc1
 * @created April 2007
 */
public class ContactList extends AbstractListModel {
    private Roster contactList;
    private static ContactList c;
    
    /** Creates a new instance of ContactList */
    public ContactList() {
        c = this;
    }
    
    /** Returns the size of contact list
     * @return contact list size
     */
    public int getSize() {
        return contactList.getEntryCount();
    }
    
    /**
     * Returns contact on the "i" possition
     * @param i position
     * @return kontact
     */
    public Object getElementAt(int i) {
        Iterator it=contactList.getEntries().iterator();
        int j=0;
        for(; it.hasNext(); j++) {
            if ( i == j )  return it.next();
            else  it.next();
        }
        return null;
    }
    
    /**
     * Sharing method
     * @return itself
     */
    public static ContactList get(){
        return c;
    }
    
    /**
     * Simply shows the contact list on monitor(to the terminal window)
     * @return StringBuffer including all of the contact list in readable form
     */
    public String toString(){
        StringBuffer sb = new StringBuffer(500);
        for(Iterator it=contactList.getEntries().iterator(); it.hasNext();)
            sb.append("" + it.next() + "\n");
        return sb.toString();
    }
    
    /**
     * Wnen the connection is established
     */
    public void setRoster(Roster roster) {
        contactList = roster;
    }
    
    /**
     * @return buddy
     */
    public RosterEntry getBuddy(String name){
        return contactList.getEntry(name);
    }
}
