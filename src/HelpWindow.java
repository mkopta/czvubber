/** HelpWindow.java */


import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

/**
 * HelpWindow is the window with the basic manual to our program.
 * It includes JFrame, JTextArea and JScrollPane.
 * 
 * @author pavel 
 * @version 1.0rc1
 * @created April 2007
 */
public class HelpWindow extends JFrame {
    
    private JFrame frame;
    private JTextArea text;
    private JScrollPane scroll;
    
    /** Creates a new instance of HelpWindow */
    public HelpWindow() {
        components();
    }
    /**
     * Crates and sets the components to the JFrame.
     */
    public void components(){
        // JFrame initialization and setup
        frame = new JFrame();
        frame.setTitle("Help!");
        frame.addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent windowEvent) {}
            public void windowClosed(WindowEvent windowEvent) {}
            public void windowClosing(WindowEvent windowEvent) {
                Register.get().switchStatusOfHelpWindow();
            }
            public void windowDeactivated(WindowEvent windowEvent) {}
            public void windowDeiconified(WindowEvent windowEvent) {}
            public void windowIconified(WindowEvent windowEvent) {}
            public void windowOpened(WindowEvent windowEvent) {}
        });
        frame.setDefaultLookAndFeelDecorated(true);
        frame.setSize(310,200);
        
        // JScrollPane inicialization and setup
        scroll = new JScrollPane();
        
        // JTextArea inicialization and setup 
        text = new JTextArea(
                "1.1 ← Help\n \n"+
                "And God said, Let there be light: \n"+
                "and there was light. \n"+
                "And God saw the light, that it was good: \n"+"" +
                "and God divided the light from the darkness. \n"+
                "And God called the light Day, \n"+"" +
                "and the darkness he called Night. \n"+
                "And there was evening and there was morning, \n"+
                "one day.\n\n"+
                "Welcome to our world!\n\n"+
                "1.2 ← Setting up connection\n"+ 
                "App -> Connect\n\n"+
                "1.3 ← Disconnecting\n"+
                "App -> Disconnect\n\n"+
                "1.4 ← Configuration\n"+
                "Configure -> Settings\n\n"+
                "1.5 ← About\n"+
                "Help -> About\n\n"+
                "1.6 ← Help\n"+
                "Help -> Help\n\n"+
                "1.7 ← Clossing\n"+
                "App -> Close");
        text.setEditable(false);
        scroll.setViewportView(text);
        
        frame.add(scroll);
        
        // making the frame visible
        frame.setVisible(true);
        Register.get().switchStatusOfHelpWindow();
    }
}