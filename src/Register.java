/** Register.java */

import java.util.ArrayList;
import org.jivesoftware.smack.*;

/**
 * Register is mainly for managing, if the window already exists(for all windows we need).
 * We do not close window while clicking on "x" bud we simply hide them and then
 * unhide them with the previous conversation.
 *
 * @author pavel
 * @version 1.0rc1
 * @created April 2007
 */
public class Register {
    private ArrayList messageWindows;
    private boolean configWindowExist, helpWindowExist;
    private static Register r;
    
    /** Creates a new instance of Register */
    public Register() {
        messageWindows = new ArrayList();
        configWindowExist = false;
        helpWindowExist = false;
        r = this;
    }
    
    /** Gets the status of configuration window */
    public boolean getStatusOfConfigWindow(){
        return configWindowExist;
    }
    /** Changes the status of configuration window */
    public void switchStatusOfConfigWindow(){
        configWindowExist=!configWindowExist;
    }
    /** Gets the status of help window */
    public boolean getStatusOfHelpWindow(){
        return helpWindowExist;
    }
    /** Changes the status of help window */
    public void switchStatusOfHelpWindow(){
        helpWindowExist=!helpWindowExist;
    }
    
    /**
     * Append the window to the window list.
     * @param messageWindow
     */
    public void appendMessageWindow(MessageWindow messageWindow){
        messageWindows.add(messageWindow);
    }
    
    /**
     * Gets the the window from the list.
     * @return window from the list
     * @param position
     */
    public MessageWindow getMessageWindow(int position){
        return (MessageWindow) messageWindows.get(position);
    }
    
    /**
     * Finds the window position in the list by the buddy name.
     * @return position of the window we are looking for
     * @param buddy
     */
    public int findMessageWindowNumberByBuddy(RosterEntry buddy){
        MessageWindow messageWindow;
        for(int i=0; i<messageWindows.size();i++){
            messageWindow = (MessageWindow)messageWindows.get(i);
            if(buddy.getUser() == messageWindow.getBuddy().getUser()){
                return i;
            }
        }
        // returns error
        return -1;
    }
    
    /**
     * Finds the message window with te findMessageWindowNumberByBuddy.
     * @param buddy
     * @return positon of the window in contact list
     */
    public MessageWindow findMessageWindowByBuddy(RosterEntry buddy){
        MessageWindow messageWindow = null;
        int position = findMessageWindowNumberByBuddy(buddy);
        if( position == -1) return messageWindow;
        else {
            return getMessageWindow(position);
        }
    }
    
    /**
     * Removes the message window from the list.
     * @param position
     */
    public void removeMessageWindow(int position){
        messageWindows.remove(position);
    }
    
    public void removeAllMessageWindow(){
        messageWindows.clear();
    }
    
    /**
     * Sharing method
     * @return itself
     */
    public static Register get(){
        return r;
    }
}
