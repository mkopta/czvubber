/**
 * GetConnect.java
 */



/** 
 * GetConnection is the class for establishing connection to the jabber server.
 *
 * @author dum8d0g
 * @version 1.0rc1
 * @created April 2007
 */
public class GetConnect {
    
    /** Method that gets the connection */
    public GetConnect() {
        Connection connection = Connection.get();
        connection.open();
    }
}
