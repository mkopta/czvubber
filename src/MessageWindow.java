/*
 * Message.java
 *
 * Created on 1. kveten 2007, 10:57
 */


/**
 * MessageWindow is the main component used for Chatting.
 * It contains Message box and Send box and its handled by
 * KeyPressed listener - for ENTER
 *
 * @author pavel
 * @version 1.0rc1
 * @created April 2007
 */

// <editor-fold defaultstate="collapsed" desc=" Imports ">
import java.awt.*;
import java.awt.Cursor;
import java.awt.event.*;
import javax.swing.*;
import java.util.EventListener;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
// </editor-fold>

public class MessageWindow extends javax.swing.JFrame implements ActionListener, EventListener{
    /** Send box scroll pane */
    private javax.swing.JScrollPane SBScroll;
    /** Message box scroll pane */
    private javax.swing.JScrollPane MBScroll;
    /** Message box */
    private JTextArea messagesBox;
    /** Sending box */
    private JTextArea sendBox;
    /** The other guy */
    private RosterEntry buddy;
    /** Pointer to itself */
    private static MessageWindow mw;
    private MessageListener messageListener, enterListener;
    
    /**
     *  Constructor of MessageWindow
     *  - creates components and activates listeners
     *  - it also sets the object buddy,
     *  which is used for sharing information about that bitch
     */
    public MessageWindow(RosterEntry buddy) {
        this.buddy = buddy;
        // makes, sets and show all of the components
        components();
        // activates listeners
        listeners();
        mw = this;
    }
    
    /**
     * Creates the chat and activates message listener for upcoming messages.
     */
    public void createChat(){
        // Gets the chat manager
        ChatManager chatManager = Connection.get().getChatManager();
        // Initialization of message listener
        messageListener = new MessageListener() {
            // Activates the message listener
            public void processMessage(Chat chat, Message message) {
                // Every upcomming message is situated in the message box window
                messagesBox.append(""+buddy.getName()+": "+message.getBody()+"\n");
                MBScroll.getVerticalScrollBar().setValue(MBScroll.getVerticalScrollBar().getMaximum());
                setVisible(true);
            }
        };
        // Creates new chat for buddy(contact)
        chatManager.createChat(buddy.getUser(), buddy.getUser(), messageListener);
    }
    
    /**
     * Activation of each components listeners.
     *  - activates keyPressed listener for sendBox
     */
    void listeners(){
        sendBox.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                // When enter is pressed.
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    try {
                        Connection.get().getChatManager().getThreadChat(buddy.getUser()).sendMessage(sendBox.getText());
                        messagesBox.append(""+Config.get().getUser()+": "+sendBox.getText()+"\n");
                        MBScroll.getVerticalScrollBar().setValue(MBScroll.getVerticalScrollBar().getMaximum());
                        sendBox.setText(null);
                        
                        MBScroll.transferFocusDownCycle();
                    } catch (XMPPException ex) {
                        System.err.println("MessageWindow.listeners():Pri odesilani zpravy nastala chyba.");
                    } catch (NullPointerException ex){
                        System.err.println("MessageWindow.listeners():Pri odesilani zpravy nastala chyba.");
                        System.err.println("MessageWindow.listeners(): threadID: "+buddy.getUser());
                        System.err.println("MessageWindow.listeners(): Chat thread in chat manager exist = "+Connection.get().getChatManager().getThreadChat(buddy.getUser())!=null+"");
                    }
                    
                }
            }
        });
    }
    
    /**
     *  Method components sets up all components of the message window
     *  - it includes SBScroll, MBScroll, sendBox and messagesBox
     */
    public void components(){
        setTitle("Chat with "+buddy.getName());
        setLayout(new BorderLayout(4,4));
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setSize(280,200);
        setContentPane(createContentPane());
        
        messagesBox = new JTextArea();
        sendBox = new JTextArea();
        SBScroll = new javax.swing.JScrollPane();
        MBScroll = new javax.swing.JScrollPane();
        
        SBScroll.setViewportView(sendBox);
        MBScroll.setViewportView(messagesBox);
        
        messagesBox.setLineWrap(true);
        messagesBox.setRows(8);
        messagesBox.setEditable(false);
        
        sendBox.setLineWrap(true);
        sendBox.setRows(2);
        sendBox.setEditable(true);
        
        add(MBScroll, BorderLayout.CENTER);
        add(SBScroll, BorderLayout.SOUTH);
        
        addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent e) {
                sendBox.requestFocusInWindow();
            }
        });
    }
    /**
     *  Creates container for JPanel
     */
    public Container createContentPane() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setOpaque(true);
        return mainPanel;
    }
    
    public void actionPerformed(ActionEvent e) {
    }
    
    /**
     * Sharing method
     * @return itself
     */
    public static MessageWindow get(){return mw;}
    
    /**
     * Tells us the JID of the buddy we are talking to
     * @return JID buddy we are tallking to
     */
    public RosterEntry getBuddy(){
        return buddy;
    }
}
