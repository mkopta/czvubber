/** MainWindow.java */


// <editor-fold defaultstate="collapsed" desc=" Imports ">
import java.awt.*;
import java.awt.event.*;
import java.util.EventListener;
import javax.swing.*;
import org.jivesoftware.smack.RosterEntry;
// </editor-fold>

/**
 * MainWindow is the main window of our program.
 * We can create chat from here, see the contacts and throw the menu get to the
 * settings, help and etc.
 * It includes JPanel, JMenuBar, JMenuItem and JList.
 *
 * @author pavel
 *
 */
public class MainWindow extends JFrame {
    // Basic panel which includes other stuff
    private JPanel mainPanel;
    // Menubar including JMenu
    private JMenuBar mainMenuBar;
    // Main application menu
    private JMenu appMenu, configureMenu, helpMenu;
    // Menu items
    private JMenuItem connectItem, disconnectItem, quitItem, settingsItem, aboutItem, helpItem;
    // List of contacts
    private JList list;
    private JScrollPane scroll;
    // Reference to itsefl
    private static MainWindow mw;
    
    /**
     * Makes main window
     */
    public MainWindow() {
        // Creates and imports all components
        createComponents();
        // Activates Listeners
        activateListeners();
        // Share it!
        mw = this;
    }
    
    /**
     * Creates and sets the components and the window.
     */
    private void createComponents() {
        // Crates JFrame
        JFrame.setDefaultLookAndFeelDecorated(true); //tohle funguje!!!
        JFrame mainFrame = new JFrame("Czvubber IM");
        // Sets the style
        mainFrame.setDefaultLookAndFeelDecorated(true);
        // Sets the operation for the x button
        mainFrame.addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent windowEvent) {}
            public void windowClosed(WindowEvent windowEvent) {}
            public void windowClosing(WindowEvent windowEvent) {
                new Quit();
            }
            public void windowDeactivated(WindowEvent windowEvent) {}
            public void windowDeiconified(WindowEvent windowEvent) {}
            public void windowIconified(WindowEvent windowEvent) {}
            public void windowOpened(WindowEvent windowEvent) {}
        });
        // Sets the frame size
        mainFrame.setSize(200, 300);
        // Sets the area where the window is located after open
        mainFrame.setLocation(100,100);
        // Adds menu bar
        mainFrame.setJMenuBar(createMainMenu());
        // Adds content pane
        mainFrame.setContentPane(createContentPane());
        // activates the JList for our contacts
        list = new JList();
        // ads the list to scroll pane
        scroll = new JScrollPane(list);
        mainFrame.add(scroll);
        // I see the end comming, I see darkness burning, I see death...
        mainFrame.setVisible(true);
    }
    
    /**
     * Function that builds main menu
     * @return mainMenuBar (JMenu)
     */
    public JMenuBar createMainMenu(){
        // Creation of all the necessary componnents
        mainMenuBar = new JMenuBar();
        appMenu = new JMenu("App");
        connectItem = new JMenuItem("Connect");
        disconnectItem = new JMenuItem("Disconnect");
        quitItem = new JMenuItem("Quit");
        configureMenu = new JMenu("Configure");
        settingsItem = new JMenuItem("Settings");
        helpMenu = new JMenu("Help");
        aboutItem = new JMenuItem("About");
        helpItem = new JMenuItem("Help");
        // Adds everything exactly where we want it ;-)
        appMenu.add(connectItem);
        appMenu.add(disconnectItem);
        appMenu.addSeparator();
        appMenu.add(quitItem);
        configureMenu.add(settingsItem);
        helpMenu.add(aboutItem);
        helpMenu.add(helpItem);
        mainMenuBar.add(appMenu);
        mainMenuBar.add(configureMenu);
        mainMenuBar.add(helpMenu);
        // Odevzdame hotovy mainMenuBar
        return mainMenuBar;
    }
    
    /**
     * Function for crating mainPanel with borderLayout.
     * @return mainPanel with borderLayout (JPanel)
     */
    public Container createContentPane() {
        // creates container for components
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setLayout(new BorderLayout());
        // i guess know...
        mainPanel.setOpaque(true);
        return mainPanel;
    }
    
    /**
     * Actiovation of listeners
     */
    public void activateListeners(){
        quitItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                new Quit();
            }
        });
        // Mouse listener for opening new chats
        list.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                // Gets the objects we need
                Connection connection = Connection.get();
                Register register = Register.get();
                // If you are connected it creates new window
                if( !connection.isNull() && connection.isConnected() ){
                    int selectedListPosition = list.getSelectedIndex();
                    RosterEntry selectedBuddy = (RosterEntry)list.getModel().getElementAt(selectedListPosition);
                    MessageWindow messageWindowWithSelectedBuddy = register.findMessageWindowByBuddy(selectedBuddy);
                    messageWindowWithSelectedBuddy.setVisible(true);
                }
            }
        });
        // About menu item Mouse listener
        aboutItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                new AboutDialog();
            }
        });
        // Settings menu item Mouse listener
        settingsItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                // TODO - prenest focus na okno
                if(!Register.get().getStatusOfConfigWindow()) new ConfigWindow().setVisible(true);
                
            }
        });
        // Connect menu item Mouse listener
        connectItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                System.out.println("Warming up..");
                new GetConnect();
                if(!Connection.get().isNull() && Connection.get().isConnected() && Connection.get().isAuthenticated()){
                    System.out.println("Connect ready.");
                    list.setModel(ContactList.get());
                } else {
                  System.out.println("Connection couldn't be established, please check out your internet connection and configuration.");
                  //new Quit();
                }
            }
        });
        // Disconnect menu item mouse listener
        disconnectItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                new GetDisconnect();
            }
        });
        // Help menu item mouse listener
        helpItem.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                if(!Register.get().getStatusOfHelpWindow()) new HelpWindow();
            }
        });
    }
    
    /** Because of implementation of the abstract methods */
    public void actionPerformed(ActionEvent actionEvent) {}
    /** Because of implementation of the abstract methods */
    public void itemStateChanged(ItemEvent e) {}
    
    /**
     * Sets the JList model in the main window
     * @param contactlist that is set as model
     */
    public void setList(ContactList contactList){
        list.setModel(contactList);
    }
    
    /**
     * Empty contact list
     */
    public void emptyList(){
        list.setListData(new String[]{});
    }
    
    /**
     * Sharing object method
     * @return objekt typu MainWindow
     */
    public static MainWindow get(){
        return mw;
    }
}
