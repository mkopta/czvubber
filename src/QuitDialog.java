/** quitDialog.java */

import javax.swing.JOptionPane;

/** 
 * For the Chief Musician. A Psalm of David. The fool hath said in his heart, There is no God. 
 * They are corrupt, they have done abominable works; There is none that doeth good. 
 * Jehovah looked down from heaven upon the children of men, To see if there were any that did 
 * understand, That did seek after God. They are all gone aside; They are together become filthy; 
 * There is none that doeth good, no, not one.
 *
 * Quit dialog window.
 * 
 * @author dum8d0g 
 * @version 1.0rc1
 * @created April 2007
 */
public class QuitDialog {
    // true result is yes user want to quit
    boolean result=false;
    
    /** Creates a new instance of quitDialog */
    public QuitDialog() {
        // Text to put on the buttons.
        String[] choices = {"Yes, I wanna kill a little puppy", "No way!"};
        int response = JOptionPane.showOptionDialog(
                null                         // Center in window.
                , "Do you really want quit?" // Message
                , "Question"                 // Title in titlebar
                , JOptionPane.YES_NO_OPTION  // Option type
                , JOptionPane.PLAIN_MESSAGE  // messageType
                , null                       // Icon (none)
                , choices                    // Button text as above.
                , "No way!"                  // Default button's label
                );
        
        // Use a switch statement to check which button was clicked.
        switch (response) {
            case 0:
                // Answer was "yes"
                result=true;
                break;
            case 1:
                // Answer was "no"
                break;
            default:
                // If we get here, something is wrong. Defensive programming!
                JOptionPane.showMessageDialog(null, "Unexpected response " + response);
        }
    }
    
    /**
     * Function to use answer from user
     * @return true if user want to quit
     */
    public boolean getResult(){
        return result;
    }
}
