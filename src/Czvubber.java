/**
 * cvubber.java
 */

import org.jivesoftware.smackx.packet.Version;

/**
 * Blessed is the man that walketh not in the counsel of the wicked, 
 * Nor standeth in the way of sinners, Nor sitteth in the seat of scoffers: 
 * But his delight is in the law of Jehovah;
 * And on his law doth he meditate day and night. 
 * And he shall be like a tree planted by the streams of water, 
 * That bringeth forth its fruit in its season, 
 * Whose leaf also doth not wither; 
 * And whatsoever he doeth shall prosper.
 * 
 * Main class - this is the main class of our messenger program. 
 * It creates objects which we need and runs new window.
 * 
 * @author dum8d0g, pavel
 * @version 1.0rc1
 * @created April 2007
 */
public class Czvubber {

  /**
   * Runs the program. 
   */
  public static void main(String[] arg) {
      System.out.println("===================================================");
      System.out.println("Czvubber is starting up..");
      /**Initialize base objects */
      new Register();
      new Config();
      new ContactList();
      new Connection();
      System.out.println("Base objects was initialized.");
      /** Show the main window */
      System.out.println("Creating main window..");
      MainWindow mainWindow = new MainWindow();
      System.out.println("Main window was created.");
      System.out.println("Czvubber is started and running.");
  }
}
