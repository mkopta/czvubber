/*
 * Config.java
 *
 * Created on 22. duben 2007, 16:13
 */


import java.util.Set;
import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

/**
 * Config class manages the configuration. The Config object keeps information
 * about the SUP configuration - Server, User and Password. This method can 
 * save/load the configuration to/from the fileo(xml).
 *
 * @author dum8d0g
 * @version 1.0rc1
 * @created April 2007
 */
public class Config {
    private final File config = new File("config.xml");
    private TransformerFactory transformerFactory;
    private Transformer transformer;
    private Document document;
    private Element root;
    private DOMSource domSource;
    private String server,user,pass,name,source;
    private static Config c;
        
    /** Creates a new instance of Config */
    public Config() {
        c = this;
        load();
    }
    /** Save configuration to file */
    public void save(){
        domSource = new DOMSource(document);
        BufferedWriter newConfig;
        try {
            newConfig = new BufferedWriter(new FileWriter(config));
            transformer.transform(domSource, new StreamResult(newConfig));
            System.out.println("Configuration was saved succsesfully.");
        } catch (IOException ex) {
            System.err.println("Failed while saving configuration!\n" + ex);
        } catch (TransformerException ex) {
            System.err.println("Failed while changing configuration!\n" + ex);
        }
    }
    /** Load configuration from file */
    public void load(){
        //opens xml and loads in to the memory
        try {
            // Definition of our file
            
            // Raders initialization
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            
            // Writters initialization
            transformerFactory =TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
            
            // Pars all document
            document = builder.parse(config);
            // takes root from the document
            root = document.getDocumentElement();
            
            // This part sets loaded data to vars
            user = root.getElementsByTagName("user").item(0).getTextContent();
            pass = root.getElementsByTagName("pass").item(0).getTextContent();
            server = root.getElementsByTagName("server").item(0).getTextContent();
            source = root.getElementsByTagName("source").item(0).getTextContent();
            System.out.println("Configuration was loaded succsesfully..");
            
        } catch (Exception e) {
            System.err.println("Failed while loading configuration!");
            System.err.println("Please create the config file first!");
            System.exit(1);
        }
        
    }
    /** Get server name from config object */
    public String getServer(){
        return server;
    }
    /** Get user name from config object */
    public String getUser(){
        return user;
    }
    /** Get password from config object */
    public String getPass(){
        return pass;
    }
    /** Get "source" string in config object */
    public String getSource(){
        return source;
    }
    /** Get JID (Example: czvubber@jabber.cz) */
    public String getJID(){
        return user+"@"+server;
    }
    /** Set user name in config object */
    public void setUser(String string){
        user = string;
        root.getElementsByTagName("user").item(0).setTextContent(string);
    }
    /** Set password in config object */
    public void setPass(String string){
        pass = string;
        root.getElementsByTagName("pass").item(0).setTextContent(string);
    }
    /** Set password in config object */
    public void setPass(char[] c) {
        String string = "";
        for(int i=0;i < c.length; i++) { //this method turns chars from JPasswordField
            string=string+c[i];          //to our desired String ;)
        }
        //pass = string;
        setPass(string);
    }
    /** Set server in config object */
    public void setServer(String string){
        server = string;
        root.getElementsByTagName("server").item(0).setTextContent(string);
    }
    /** Set "source" string in config object */
    public void setSource(String string){
        source = string;
    }
    /** Get config object */
    public static Config get(){
        return c;
    }
    
    
}
