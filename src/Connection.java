/** Connection.java */

import java.net.ConnectException;
import java.util.Iterator;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.ChatState;
import org.jivesoftware.smackx.ChatStateListener;

/**
 * Connection is mainly for logging to the jabber server. For the connection
 * itself is used the getConnect class. Method open() takes care about
 * the connection, login and geting roster to contact list. It also
 * establish new Message listener and prepares the chat windows.
 * 
 * @author dum8d0g
 * @version 1.0rc1
 * @created April 2007 
 */
public class Connection {
    private Config config;
    private static Connection conn;
    public XMPPConnection connection;
    private ChatManager chatManager;
    
    /** Creates a new instance of Connection */
    public Connection() {
        // config needed
        config = Config.get();
        conn = this;
    }
    
    /**
     * Login to the server with the username and password.
     */
    public void open(){
        try {
            System.out.println("1");
            connection = new XMPPConnection(config.getServer());
            connection.connect();
            System.out.println("User: "+ config.getJID());
            System.out.println("Pass: "+ config.getPass());
            System.out.println("Src:  "+ config.getSource());
            connection.login(config.getJID(), config.getPass(), config.getSource());
            chatManager = connection.getChatManager();
            ContactList.get().setRoster(connection.getRoster());
            MainWindow.get().setList(ContactList.get());
            prepareChatWindows();
            newMessagesListener();
        } catch (Exception ex) {
            new ConnectionErrorDialog();
        }
    }
    
    /**
     * Checkout if the object isn't empty.
     * @return true while the connection is empty
     */
    public boolean isNull(){
        return connection==null;
    }
    
    /** Close connection. */
    public void close(){
        if(connection.isConnected()) connection.disconnect();
    }
    
    /**
     * Says if we are, or we aren't connected.
     * @return true if the connection is established
     */
    public boolean isConnected(){
        return connection.isConnected();
    }
    
    /**
     * Says if we are logged in, or not.
     * @return true when we are logged in
     */
    public boolean isAuthenticated(){
        return connection.isAuthenticated();
    }
    
    /**
     * Object sharing method.
     * @return connection object.
     */
    public static Connection get(){
        return conn;
    }
    
    /**
     * @return chatManager
     */
    public ChatManager getChatManager(){
        return chatManager;
    }
    
    /*
     * Activates the listener for new chats.
     */
    private void newMessagesListener() {
        connection.getChatManager().addChatListener(new ChatManagerListener() {
            public void chatCreated(Chat chat, boolean b) {
                if(!b){
                    String celeJID = chat.getParticipant();
                    int konec = celeJID.indexOf("/");
                    String name = celeJID.substring(0,konec);
                    System.out.println("Oriznute: "+name);
                    ContactList contactList = ContactList.get();
                    RosterEntry buddy = contactList.getBuddy(name);
                    if (buddy == null) System.err.println("Pise nejaky cizinec!");
                    else {
                        Register register = Register.get();
                        MessageWindow messageWindow = register.get().findMessageWindowByBuddy(buddy);
                        messageWindow.setVisible(true);
                    }
                }
            }
        });
    }
    /**
     * Pre-prepares the chat windows in hidden mode. ;)
     */
    public void prepareChatWindows(){
        Register register = Register.get();
        ContactList contactList = ContactList.get();
        int contactListSize = contactList.getSize();
        RosterEntry buddy;
        for (int positionInList = 0; positionInList < contactListSize; positionInList++) {
            buddy = (RosterEntry)contactList.getElementAt(positionInList);
            MessageWindow newMessageWindow = new MessageWindow(buddy);
            newMessageWindow.createChat();
            register.appendMessageWindow(newMessageWindow);
        }
    }
}
