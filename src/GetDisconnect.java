/**
 * GetDisconnect.java
 */



/** 
 * GetDisconnect is our class for closing the connection to the jabber server.
 * 
 * @author dum8d0g
 * @version 1.0rc1
 * @created April 2007
 */
public class GetDisconnect {
    
    /** Creates an instance for disconnecting */
    public GetDisconnect() {
        System.out.println("Getting disconnect..");
        /** We create new obcject connection */
        Connection connection = Connection.get();
        /** When its empty, we are disconnected */
        if( connection.isNull()) System.out.println("Not connected!");
        // When its full, we are connected and the program proceed whith disconnection
        else if( connection.isConnected() ){
            connection.close();
            MainWindow.get().emptyList();
            Register.get().removeAllMessageWindow();
            System.out.println("Disconnected.");
        }
        // Else we aren't connected
        else System.out.println("Not connected!");
    }
    
}
